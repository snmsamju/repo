package FrameWork;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class ScreenShotUtility {
	public static void captureScreenShot(WebDriver driver, String screenshotName)

	{
		try {
			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(src,
					new File("C:\\ChromeDriver\\ProgressiveScreenShots\\Screenshots\\" + screenshotName + ".Jpeg"));
			Thread.sleep(2000);
			System.out.println("Screenshot Taken");

		} catch (Exception e) {

			System.out.println("Exception while taking screenshot" + e.getMessage());
		}

	}

}
