package ProgressivePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SnapShotQuote {
	WebDriver driver = null;

	public SnapShotQuote (WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@id='SnapshotEnrollment40Edit_embedded_questions_list_SnapshotPolicyEnrollment_Y']")
	public WebElement SelectSnapshotEnrollmentSignUp;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
	public WebElement SelectContinue;


	public void SelectSnapshotEnrollmentSignUp() {
		SelectSnapshotEnrollmentSignUp.click();
	}
	public void SelectContinue() {
		SelectContinue.click();
	
	}
}

