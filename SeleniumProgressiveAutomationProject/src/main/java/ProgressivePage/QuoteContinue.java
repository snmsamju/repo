package ProgressivePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class QuoteContinue {
	WebDriver driver = null;

	public  QuoteContinue(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
	public WebElement SelectContinue;

	public void SelectContinue() {
		SelectContinue.click();

	}
}
