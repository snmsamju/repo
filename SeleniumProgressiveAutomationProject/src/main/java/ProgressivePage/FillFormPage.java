package ProgressivePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class FillFormPage {
	WebDriver driver = null;

	public FillFormPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@id='NameAndAddressEdit_embedded_questions_list_FirstName'][1]")
	public WebElement FirstName;

	@FindBy(how = How.XPATH, using = "//input[@id='NameAndAddressEdit_embedded_questions_list_MiddleInitial']")
	public WebElement MiddleName;

	@FindBy(how = How.XPATH, using = "//input[@id='NameAndAddressEdit_embedded_questions_list_LastName']")
	public WebElement LastName;

	@FindBy(how = How.XPATH, using = "//select[@id='NameAndAddressEdit_embedded_questions_list_Suffix']")
	public WebElement Suffix;

	@FindBy(how = How.XPATH, using = "//input[@id='NameAndAddressEdit_embedded_questions_list_DateOfBirth']")
	public WebElement DateofBirth;

	@FindBy(how = How.XPATH, using = "//input[@id='NameAndAddressEdit_embedded_questions_list_MailingAddress'][1]")
	public WebElement StreetNoName;

	@FindBy(how = How.XPATH, using = "//input[@id='NameAndAddressEdit_embedded_questions_list_ApartmentUnit']")
	public WebElement AppNO;

	@FindBy(how = How.XPATH, using = "//input[@id='NameAndAddressEdit_embedded_questions_list_City'][1]")
	public WebElement City;

	@FindBy(how = How.XPATH, using = "//input[@id='NameAndAddressEdit_embedded_questions_list_MailingZipType'][1]")
	public WebElement POBox;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Okay, start my quote.')]")
	public WebElement OkayStartMyQuote;

	public void FirstName(String enterfirst) {
		FirstName.sendKeys(enterfirst);
	}

	public void MiddleName(String entermiddle) {
		MiddleName.sendKeys(entermiddle);
	}

	public void LastName(String enterlast) {
		LastName.sendKeys(enterlast);
	}

	public void Suffix(String entersuffix) {
		Suffix.sendKeys(entersuffix);
	}

	public void DateofBirth(String enterdateofBirth) {
		DateofBirth.sendKeys(enterdateofBirth);
	}

	public void StreetNoName(String enterstreetNoName) {
		StreetNoName.sendKeys(enterstreetNoName);
	}

	public void AppNO(String enterappNo) {
		AppNO.sendKeys(enterappNo);
	}

	public void City(String entercity) {
		City.sendKeys(entercity);
	}
	public void POBox() {
		POBox.click();
	}

	public void OkayStartMyQuote() {
		OkayStartMyQuote.click();
}
}
