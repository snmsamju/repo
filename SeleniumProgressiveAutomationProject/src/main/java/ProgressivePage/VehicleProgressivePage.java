package ProgressivePage;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class VehicleProgressivePage {
	WebDriver driver = null;

	public VehicleProgressivePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void selectYear(String year) {
		WebElement yearWebelement = driver.findElement(By.xpath("//*[@name='VehiclesNew_embedded_questions_list_Year']"));
		List<WebElement> YearList = yearWebelement.findElements(By.tagName("li"));
		for(WebElement selectYear : YearList) {
			if(selectYear.getText().equalsIgnoreCase(year)) {
				selectYear.click();
				break;
			}
		}
	}
	public void selectMake(String make) {
		WebElement makeWebelement = driver.findElement(By.xpath("//*[@name='VehiclesNew_embedded_questions_list_Make']"));
		List<WebElement> MakeList = makeWebelement.findElements(By.tagName("li"));
		for(WebElement selectMake : MakeList) {
			if(selectMake.getText().equalsIgnoreCase(make)) {
				selectMake.click();
				break;
			}
		}
	}
	public void selectModel(String model) throws InterruptedException {
		WebElement modelWebelement = driver.findElement(By.xpath("//*[@name='VehiclesNew_embedded_questions_list_Model']"));
		List<WebElement> ModelList = modelWebelement.findElements(By.tagName("li"));
		for(WebElement selectModel : ModelList) {
			if(selectModel.getText().equalsIgnoreCase(model)) {
				selectModel.click();
				Thread.sleep(3000);
				break;
			}
		}
	}
	

	@FindBy(how = How.XPATH, using = "//select[@id='VehiclesNew_embedded_questions_list_BodyStyle']")
	public WebElement bodyType;

	@FindBy(how = How.XPATH, using = "//select[@id='VehiclesNew_embedded_questions_list_VehicleUse']")
	public WebElement primaryUse;

	@FindBy(how = How.XPATH, using = "//input[@id='VehiclesNew_embedded_questions_list_VehicleTransportNetworkCompanyIndicator']")
	public WebElement ClickOnRideSharing;

	@FindBy(how = How.XPATH, using = "//select[@id='VehiclesNew_embedded_questions_list_OwnOrLease']")
	public WebElement ownOrLease;

	@FindBy(how = How.XPATH, using = "//select[@id='VehiclesNew_embedded_questions_list_LengthOfOwnership']")
	public WebElement ownershipPeriod;
	
	//@FindBy(how = How.XPATH, using = "//input[@id='VehiclesNew_embedded_questions_list_AutomaticEmergencyBraking_Y']")
	//public WebElement automaticEmergencyBreak;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Done')]")
	public WebElement ClickOnDone;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
	public WebElement ClickOnContinue;

	public void selectBodyType(String BodyType) {
		Select chooseBodyType = new Select(bodyType);
		chooseBodyType.selectByVisibleText(BodyType);
	}

	public void selectPrimaryUse(String PrimaryUse) {
		Select chooseprimaryUse = new Select(primaryUse);
		chooseprimaryUse.selectByVisibleText(PrimaryUse);
	}

	public void ClickRideSharing() {
		ClickOnRideSharing.click();
	}

	public void selectOwnOrLease(String ownLease) {
		Select chooseOwnLease = new Select(ownOrLease);
		chooseOwnLease.selectByVisibleText(ownLease);
	}

	public void selectOwnershipPeriod(String Period) {
		Select chooseOwnershipPeriod = new Select(ownershipPeriod);
		chooseOwnershipPeriod.selectByVisibleText(Period);
	}

	//public void selectAutomaticEmergencyBreak() {
	//	automaticEmergencyBreak.c
	//}
	public void ClickOnDone() {
		ClickOnDone.click();
	}

	public void ClickOnContinue() {
		ClickOnContinue.click();
	}
}

