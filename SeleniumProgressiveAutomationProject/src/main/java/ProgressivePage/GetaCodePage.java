package ProgressivePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import FrameWork.FrameWorkPage;

public class GetaCodePage {
	WebDriver driver = null;
	public static HomePage homePage;
	public static FrameWorkPage frameWork;

	public GetaCodePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public static void main(String[] args) {
		homePage.clickOnAuto();
		if (FrameWorkPage.isAlertPresent(homePage.Auto)) {

		}
	}

	@FindBy(how = How.XPATH, using = "//input[@id='zipCode_overlay']")
	public WebElement zipCode;

	@FindBy(how = How.XPATH, using = "//input[@name='qsButton']")
	public WebElement clickGetaCode;

	public void getACode(String enterzip) {
		zipCode.sendKeys(enterzip);
		clickGetaCode.click();

	}

}
