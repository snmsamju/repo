package ProgressivePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class PersonalDetailsPage {
	WebDriver driver = null;

	public PersonalDetailsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = " //input[@id='DriversAddPniDetails_embedded_questions_list_Gender_M']")
	public WebElement SelectGender;

	@FindBy(how = How.XPATH, using = " //select[@id='DriversAddPniDetails_embedded_questions_list_MaritalStatus']")
	public WebElement SelectMaritalStatus;

	@FindBy(how = How.XPATH, using = " //select[@id='DriversAddPniDetails_embedded_questions_list_HighestLevelOfEducation']")
	public WebElement SelectHighestLevelofEducation;

	@FindBy(how = How.XPATH, using = "//select[@id='DriversAddPniDetails_embedded_questions_list_EmploymentStatus']")
	public WebElement SelectEmploymentStatus;

	@FindBy(how = How.XPATH, using = "//input[@id='DriversAddPniDetails_embedded_questions_list_SocialSecurityNumber']")
	public WebElement EnterSocialSecurityNumber;

	@FindBy(how = How.XPATH, using = "//select[@id='DriversAddPniDetails_embedded_questions_list_PrimaryResidence']")
	public WebElement SelectPrimaryResidence;

	@FindBy(how = How.XPATH, using = "//select[@id='DriversAddPniDetails_embedded_questions_list_HasPriorAddress']")
	public WebElement SelectHasPriorAddress;

	@FindBy(how = How.XPATH, using = " //select[@id='DriversAddPniDetails_embedded_questions_list_LicenseStatus']")
	public WebElement SelectLicenseStatus;

	@FindBy(how = How.XPATH, using = "//select[@id='DriversAddPniDetails_embedded_questions_list_DriverYearsLicensed']")
	public WebElement SelectDriverYearsLicensed;

	@FindBy(how = How.XPATH, using = "//input[@id='DriversAddPniDetails_embedded_questions_list_HasAccidentsOrClaims_N']")
	public WebElement SelectAccidentsOrClaims;

	@FindBy(how = How.XPATH, using = "//input[@id='DriversAddPniDetails_embedded_questions_list_HasTicketsOrViolations_N']")
	public WebElement SelectTicketsOrViolations;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
	public WebElement SelectContinue;
	
	public void SelectGender() {
		SelectGender.click();
	}

	public void SelectMaritalStatus(String choosemaritalStatus) {
		SelectMaritalStatus.sendKeys(choosemaritalStatus);
	}

	public void SelectHighestLevelofEducation(String chooselevelofEdu) {
		SelectHighestLevelofEducation.sendKeys(chooselevelofEdu);
	}

	public void SelectEmploymentStatus(String chooseempStatus) {
		SelectEmploymentStatus.sendKeys(chooseempStatus);
	}

	public void EnterSocialSecurityNumber(String enterSSN) {
		EnterSocialSecurityNumber.sendKeys(enterSSN);
	}

	public void SelectPrimaryResidence(String selectPriRes) {
		SelectPrimaryResidence.sendKeys(selectPriRes);
	}

	public void SelectHasPriorAddress(String selectPriAdd) {
		SelectHasPriorAddress.sendKeys(selectPriAdd);
	}

	public void SelectLicenseStatus(String selectLicStatus) {
		SelectLicenseStatus.sendKeys(selectLicStatus);
	}

	public void SelectDriverYearsLicensed(String selectDYL) {
		SelectDriverYearsLicensed.sendKeys(selectDYL);
	}

	public void SelectAccidentsOrClaims() {
		 SelectAccidentsOrClaims.click();
	}

	public void SelectTicketsOrViolations() {
		SelectTicketsOrViolations.click();
	}

	public void SelectContinue() {
		SelectContinue.click();
	}
}
