package ProgressivePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AutoInsuranceHistoryPage {
	WebDriver driver = null;

	public AutoInsuranceHistoryPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@id='FinalDetailsEdit_embedded_questions_list_InsuranceToday_N']")
	public WebElement ClickAutoInsuranceToday;

	@FindBy(how = How.XPATH, using = "//input[@id='FinalDetailsEdit_embedded_questions_list_InsuranceLastMonth_N']")
	public WebElement ClickAutoInsuranceLastMonth;

	@FindBy(how = How.XPATH, using = "//input[@id='FinalDetailsEdit_embedded_questions_list_OtherPolicies_N']")
	public WebElement ClickOtherPolicies;


	@FindBy(how = How.XPATH, using = "//input[@id='FinalDetailsEdit_embedded_questions_list_PrimaryEmailAddress']")
	public WebElement EnterPrimaryEmailAddress;

	@FindBy(how = How.XPATH, using = "	//input[@id='FinalDetailsEdit_embedded_questions_list_HealthInsurance_Y']")
	public WebElement ClickHealthInsuranceYN;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
	public WebElement ClickOnaContinue;
	

	public void ClickAutoInsuranceToday() {
		ClickAutoInsuranceToday.click();
	}

	public void SelectAutoInsuranceLastMonth() {
		ClickAutoInsuranceLastMonth.click();
	}


	public void ClickOtherPolicies() {
		ClickOtherPolicies.click();

	}

	public void EnterPrimaryEmailAddress(String enterEmailAddress) {
		EnterPrimaryEmailAddress.sendKeys(enterEmailAddress);

	}

	public void ClickHealthInsuranceYN() {
		ClickHealthInsuranceYN.click();

	}
	public void ClickOnaContinue() {
		ClickOnaContinue.click();
	}
}

