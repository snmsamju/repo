package ProgressiveTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import FrameWork.ScreenShotUtility;
import ProgressivePage.FillFormPage;
import ProgressivePage.GetaCodePage;
import ProgressivePage.HomePage;
import ProgressivePage.PersonalDetailsPage;
import ProgressivePage.QuoteContinue;
import ProgressivePage.RateProgressivePage;
import ProgressivePage.SnapShotQuote;
import ProgressivePage.VehicleProgressivePage;

public class ProgressiveTestDemo {
	static WebDriver driver;

	@BeforeTest
	public static void setup() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();

		driver.get("http://www.progressive.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.navigate().refresh();
		ScreenShotUtility.captureScreenShot(driver, "Progressive Home Page");
		System.out.println(driver.getTitle() + "....... was Launched");
	}

	@Test(priority = 0)
	public static void clickOnAutolink() {
		HomePage homePage = new HomePage(driver);
		homePage.clickOnAuto();
	}

	@Test(priority = 1)
	public static void CodeandQuote() throws InterruptedException {
		GetaCodePage getaCode = new GetaCodePage(driver);
		Thread.sleep(3000);
		getaCode.getACode("22044");
		ScreenShotUtility.captureScreenShot(driver, "CodeGeneratePage screenshot");

	}

	@Test(priority = 2)
	public static void fillForm() throws InterruptedException {

		FillFormPage fillForm = new FillFormPage(driver);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		fillForm.FirstName("Sanam");
		Thread.sleep(1000);
		fillForm.MiddleName("K");
		Thread.sleep(1000);
		fillForm.LastName("Maharjan");
		Thread.sleep(1000);
		fillForm.Suffix("Sr");
		Thread.sleep(1000);
		fillForm.DateofBirth("11/09/1988");

		// USING TAB KEYS
		Actions act = new Actions(driver);
		act.sendKeys(Keys.TAB).build().perform();
		act.sendKeys(Keys.RETURN).build().perform();

		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		fillForm.StreetNoName("6001 Arlington Blvd");
		Thread.sleep(2000);
		fillForm.AppNO("700");
		Thread.sleep(2000);
		fillForm.City("Falls Church");
		Thread.sleep(2000);
		fillForm.POBox();
		Thread.sleep(2000);
		fillForm.OkayStartMyQuote();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		ScreenShotUtility.captureScreenShot(driver, "PersonnelDetailsPage screenshot");
	}

	@Test(priority = 3)
	public static void selectVehicleProgressive() throws InterruptedException {
		Thread.sleep(3000);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		VehicleProgressivePage vehicleProgressive = new VehicleProgressivePage(driver);
		Thread.sleep(3000);
		vehicleProgressive.selectYear("2018");
		Thread.sleep(2000);
		vehicleProgressive.selectMake("Honda");
		Thread.sleep(2000);
		vehicleProgressive.selectModel("Civic");
		Thread.sleep(2000);
		vehicleProgressive.selectBodyType("2DR 4CYL");
		Thread.sleep(3000);
		vehicleProgressive.selectPrimaryUse("Personal (to/from work or school, errands, pleasure)");
		Thread.sleep(2000);
		vehicleProgressive.ClickRideSharing();
		Thread.sleep(2000);
		vehicleProgressive.selectOwnOrLease("Own");
		Thread.sleep(2000);
		vehicleProgressive.selectOwnershipPeriod("1 month - 1 year");
		Thread.sleep(2000);

		Thread.sleep(2000);
		vehicleProgressive.ClickOnContinue();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		ScreenShotUtility.captureScreenShot(driver, "VehiclesDetailsPage screenshot");
	}

	@Test(priority = 4)
	public static void selectDriversDetail() throws InterruptedException {
		Thread.sleep(3000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		PersonalDetailsPage driverDetails = new PersonalDetailsPage(driver);
		Thread.sleep(2000);
		driverDetails.SelectGender();
		driverDetails.SelectMaritalStatus("Single");
		Thread.sleep(3000);
		driverDetails.SelectHighestLevelofEducation("Graduate work or graduate degree");
		Thread.sleep(2000);
		driverDetails.SelectEmploymentStatus("Not working/other");
		Thread.sleep(2000);
		driverDetails.EnterSocialSecurityNumber("123456789");
		Thread.sleep(2000);
		driverDetails.SelectPrimaryResidence("Rent");
		Thread.sleep(2000);
		driverDetails.SelectHasPriorAddress("No");
		Thread.sleep(2000);
		driverDetails.SelectLicenseStatus("Valid");
		Thread.sleep(2000);
		driverDetails.SelectDriverYearsLicensed("Less than 1 year");
		Thread.sleep(2000);
		driverDetails.SelectAccidentsOrClaims();
		Thread.sleep(2000);
		driverDetails.SelectTicketsOrViolations();
		Thread.sleep(2000);
		driverDetails.SelectContinue();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		ScreenShotUtility.captureScreenShot(driver, "DriversDetailsPage screenshot");

	}

	@Test(priority = 5)
	public static void SelectQouteContinue() throws InterruptedException {
		Thread.sleep(3000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		QuoteContinue DriverQouteContinue = new QuoteContinue(driver);
		Thread.sleep(2000);
		DriverQouteContinue.SelectContinue();
		ScreenShotUtility.captureScreenShot(driver, "QuoteContinuePage screenshot");

	}

	@Test(priority = 6)
	public void SelectAutoInsurance() throws InterruptedException {
		Thread.sleep(3000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		ProgressivePage.AutoInsuranceHistoryPage AutoInsuranceHistory = new ProgressivePage.AutoInsuranceHistoryPage(
				driver);
		Thread.sleep(3000);
		AutoInsuranceHistory.ClickAutoInsuranceToday();
		Thread.sleep(3000);
		AutoInsuranceHistory.SelectAutoInsuranceLastMonth();
		Thread.sleep(3000);

		AutoInsuranceHistory.ClickOtherPolicies();
		Thread.sleep(3000);
		AutoInsuranceHistory.EnterPrimaryEmailAddress("snmpanga2015@gmail.com");
		Thread.sleep(3000);
		AutoInsuranceHistory.ClickHealthInsuranceYN();
		Thread.sleep(3000);
		AutoInsuranceHistory.ClickOnaContinue();
		ScreenShotUtility.captureScreenShot(driver, "AutoInsuranceDetailsPage screenshot");
	}

	@Test(priority = 7)
	public void Snapshot() throws InterruptedException {
		Thread.sleep(3000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		SnapShotQuote Snapshot = new SnapShotQuote(driver);
		Thread.sleep(2000);
		Snapshot.SelectSnapshotEnrollmentSignUp();
		Thread.sleep(2000);
		Snapshot.SelectContinue();
		ScreenShotUtility.captureScreenShot(driver, "SnapshotPage screenshot");

	}

	@Test(priority = 8)
	public void RateProgressPage() throws InterruptedException {
		Thread.sleep(3000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		RateProgressivePage RateProgressive = new RateProgressivePage(driver);
		RateProgressive.ClickOnNothanksjustauto();
		ScreenShotUtility.captureScreenShot(driver, "RateProgressDetailsPage screenshot");
	}

	@Test(priority = 9)
	public static void validQuote() {
		if (driver.findElement(By.xpath("(//button[contains(text(), 'Finish & Buy')])[1]")).getText()
				.contains("Finish & Buy")) {
			ScreenShotUtility.captureScreenShot(driver, "Final quote screenshot");
			System.out.println("Pass");
		} else {
			System.out.println("Fail");
		}
	}

	@AfterTest
	public void tearDown() {
		driver.close();
		driver.quit();
		System.out.println("Project Completed");
		System.out.println();

	}
}
