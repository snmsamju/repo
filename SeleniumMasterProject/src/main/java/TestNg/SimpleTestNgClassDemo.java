package TestNg;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.GoogleObjects;

public class SimpleTestNgClassDemo {

	// Class Level Variable
	static WebDriver driver;

	@BeforeTest
	public void setUpTest() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();

	}

	public static void invokedBrowser() {
		driver.navigate().to("http://www.google.com");
		driver.navigate().refresh();

		System.out.println(driver.getTitle() + "------------------------ Was launched");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test(priority = 0)
	public static void performsActions() {
//navigate to google.com
		invokedBrowser();
		
		GoogleObjects obj = new GoogleObjects();
		obj.GoogleSearch("What is TestNg");
	}
	@AfterTest
	public static void terminateTest() {
		driver.close();
		driver.quit();
	}
}
