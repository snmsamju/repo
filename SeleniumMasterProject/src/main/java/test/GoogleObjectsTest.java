package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.GoogleObjects;

public class GoogleObjectsTest {

	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		invokeBrowser();
		performActions();
		tearDownTest();
	}

//1.Invoke the Browser
	public static void invokeBrowser() throws InterruptedException {
//Set the system path to point to the Chromedriver.exe file
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
//Utilizing the driver global variable and creating a new chrome driver
		driver = new ChromeDriver();
		// Navigate to Google Website
		driver.navigate().to("http://www.google.com");
		// maximize the window
		driver.manage().window().maximize();
	}

//2.Perform Actions
	public static void performActions() {
		GoogleObjects obj = new GoogleObjects();
		obj.GoogleSearch("what is Page Factory");
	}

//3. End Test Case
	public static void tearDownTest() {
		driver.close();
		driver.quit();
	}

}
