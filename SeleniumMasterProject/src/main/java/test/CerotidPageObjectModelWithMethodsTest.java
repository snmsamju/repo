package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.CerotidPageObjectModelWithMethods;

public class CerotidPageObjectModelWithMethodsTest {

	// Class level variable
	public static WebDriver driver = null;

	public static void main(String[] args) {

		invokeBrowser();
		completeForm();
	}

	// 1: Invoke Browser
	public static void invokeBrowser() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();

		driver.get("http://www.cerotid.com");
		// Maximize the window
		driver.manage().window().maximize();
	}

	/// Complete the Form
	public static void completeForm() {
		// Create object to access our page class
		CerotidPageObjectModelWithMethods obj = new CerotidPageObjectModelWithMethods(driver);
		// Utilizing the object created to choose course
		obj.selectCourse("QA Automation");
		// Utilizing the object created to choose session
		obj.selectSession("Upcoming Session");
		// Utilizing the object to enter name/address/city/state/zip
		obj.enterName("kobe Bryant");
		obj.enterAddress("703 calredron ");
		obj.enterCity("Betestha");
		obj.selectState("MD");
		obj.enterZip("20181");
		obj.enterEmail("msanam0143@gmail.com");
		obj.enterPhone("2403008675");
		obj.selectVisaStatus("F1");
		obj.selectMediaSource("Social Media");
		obj.enterRelocate();
		obj.enterEduDetails("BBS Onward");

	}
}
