package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import pages.CerotidWebsitePageElements;

//Created to test the page objects in CerotidWebsite
public class CerotidWebsitePageElementsTest {

	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		invokeBrowserCerotidPage();
		fillForm();

	}

	// step 1:
	public static void invokeBrowserCerotidPage() throws InterruptedException {
		// Set the system path to point to the chromedriver.exe file
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		
		// Utilizing the driver global variable and creating a new chrome driver
		driver = new ChromeDriver();

		// Navigate to Cerotid Website
		driver.navigate().to("http://www.cerotid.com");
		// Maximize the window
		driver.manage().window().maximize();

		// Waite example
		TimeUnit.SECONDS.sleep(4);

		// Refreshing the screen
		driver.navigate().refresh();

		// Adding some consition
		String titleName = "Cerotid";
		if (driver.getTitle().contains(titleName)) {
			// Prints the title
			System.out.println(driver.getTitle() + "--------------- Was Launched");
		} else {
			System.out.println("Excepted title " + titleName + " was not seen");
			System.out.println("Test Fail -------- Ending Test");
			driver.quit();
			driver.close();
		}
	}

	// step 2:
	public static void fillForm() {
		Select chooseCourse = new Select(CerotidWebsitePageElements.selectCourse(driver));
		chooseCourse.selectByVisibleText("QA Automation");

		Select chooseSession = new Select(CerotidWebsitePageElements.chooseSession(driver));
		chooseSession.selectByVisibleText("Upcoming Session");

		// Entering Name
		CerotidWebsitePageElements.enterName(driver).sendKeys("John Doe");
		// Entering Address
		CerotidWebsitePageElements.inputAddress(driver).sendKeys("10256 Address somewhere");
		// Entering City
		CerotidWebsitePageElements.inputCity(driver).sendKeys("Lee Highway");
		// Entering State
		Select chooseState = new Select(CerotidWebsitePageElements.chooseState(driver));
		chooseState.selectByVisibleText("VA");
		// Entering Zip Code
		CerotidWebsitePageElements.inputZipCode(driver).sendKeys("22044");
		// Entering Email
		CerotidWebsitePageElements.inputEmail(driver).sendKeys("msanam0143@gmail.com");
		// Entering Phone
		CerotidWebsitePageElements.inputPhone(driver).sendKeys("1234567899");

		Select chooseVisaStatus = new Select(CerotidWebsitePageElements.chooseVisaStatus(driver));
		chooseVisaStatus.selectByVisibleText("OPT");

		Select chooseMediaSource = new Select(CerotidWebsitePageElements.chooseMediaSource(driver));
		chooseMediaSource.selectByVisibleText("Other");
		// Relocate yesorNo
		CerotidWebsitePageElements.inputYesorNo(driver).sendKeys("No");
		// Text area to write
		CerotidWebsitePageElements.inputText(driver).sendKeys("Completed MBS");
	}

	// step 3:
}
