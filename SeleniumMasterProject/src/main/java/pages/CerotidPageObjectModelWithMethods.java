
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CerotidPageObjectModelWithMethods {

	// Class level Variable
	WebDriver driver = null;

	// Creating By object and storing finding elements by xpath
	By selectCourse = By.xpath("//select[@id='classType']");
	By selectSession = By.xpath("//select[@id='sessionType']");
	By enterName = By.xpath("(//input[@data-validation-required-message='Please enter your name.'])[1]");
	By enterAddress = By.xpath("//input[@id='address']");
	By enterCity = By.xpath("//input[@id='city']");
	By selectState = By.xpath("//select[@id='state']");
	By enterZip = By.xpath("//input[@id='zip']");
	By enterEmail = By.xpath("//input[@id='email']");
	By enterPhone = By.xpath("//input[@id='phone']");
	By selectVisaStatus = By.xpath("//select[@id='visaStatus']");
	By selectMediaSource = By.xpath("//select[@id='mediaSource']");
	By enterRelocate = By.xpath("(//input[@id='relocate'])[2]");
	By enterText = By.xpath("//textarea[@id='eduDetails']");

	public CerotidPageObjectModelWithMethods(WebDriver driver) {
		this.driver = driver;
	}

	// Choose Course
	public void selectCourse(String courseName) {
		// creating new webelement object
		WebElement element = driver.findElement(selectCourse);
		Select chooseCourse = new Select(element);
		chooseCourse.selectByVisibleText(courseName);
	}

	// Choose Session
	public void selectSession(String sessionName) {
		// creating new webelement object
		WebElement element = driver.findElement(selectSession);
		Select chooseSession = new Select(element);
		chooseSession.selectByVisibleText(sessionName);
	}

	// Enter Name
	public void enterName(String name) {
		driver.findElement(enterName).sendKeys(name);
	}

	// Enter Address
	public void enterAddress(String address) {
		driver.findElement(enterAddress).sendKeys(address);

	}

	// Enter City
	public void enterCity(String city) {
		driver.findElement(enterCity).sendKeys(city);

	}

	// Choose State
	public void selectState(String stateName) {
		// creating new webelement object
		WebElement element = driver.findElement(selectState);
		Select chooseState = new Select(element);
		chooseState.selectByVisibleText(stateName);
	}

	// Enter Zip Code
	public void enterZip(String zip) {
		driver.findElement(enterZip).sendKeys(zip);
	}
	// Enter Email Id
		public void enterEmail(String email) {
			driver.findElement(enterEmail).sendKeys(email);
		}

	// Enter Phone
	public void enterPhone(String phone) {
		driver.findElement(enterPhone).sendKeys(phone);
	}

	// Choose Visa Status
	public void selectVisaStatus(String VisaStatusName) {
		WebElement element = driver.findElement(selectVisaStatus);
		Select chooseVisaStatus = new Select(element);
		chooseVisaStatus.selectByVisibleText(VisaStatusName);

	}

	// Choose Media Source
	public void selectMediaSource(String MediaSourceName) {
		WebElement element = driver.findElement(selectMediaSource);
		Select chooseMediaSource = new Select(element);
		chooseMediaSource.selectByVisibleText(MediaSourceName);

	}

	// Enter Relocate
	public void enterRelocate() {
		driver.findElement(enterRelocate).click();
	}

	// Enter Edu details
	public void enterEduDetails(String eduDetails) {
		driver.findElement(enterText).sendKeys(eduDetails);
	}

}



