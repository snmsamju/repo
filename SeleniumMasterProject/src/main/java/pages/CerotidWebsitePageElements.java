package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

// Purpose of this class is to return WebElements

public class CerotidWebsitePageElements {
//Global Variable
	static WebElement element;

	// Course Element
	public static WebElement selectCourse(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='classType']"));
		return element;
	}

	// Session Element
	public static WebElement chooseSession(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='sessionType']"));
		return element;
	}

	// Full name Element
	public static WebElement enterName(WebDriver driver) {

		element = driver.findElement(By.xpath("(//input[@data-validation-required-message='Please enter your name.'])[1]"));
		return element;
	}

	// Address Element
	public static WebElement inputAddress(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='address']"));
		return element;
	}

	// City Element
	public static WebElement inputCity(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='city']"));
		return element;
	}

	// State Element
	public static WebElement chooseState(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='state']"));
		return element;
	}

	// Zip Code Element
	public static WebElement inputZipCode(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='zip']"));
		return element;
	}

	// Email Element
	public static WebElement inputEmail(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='email']"));
		return element;
	}

	// Phone Element
	public static WebElement inputPhone(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='phone']"));
		return element;
	}

	// VisaStatus Element
	public static WebElement chooseVisaStatus(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='visaStatus']"));
		return element;
	}

	// MediaSource Element
	public static WebElement chooseMediaSource(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='mediaSource']"));
		return element;
	}

	// relocate Element
	public static WebElement inputYesorNo(WebDriver driver) {
		element = driver.findElement(By.xpath("(//input[@id='relocate'])[2]"));
		return element;
	}

	// textarea to write Element
	public static WebElement inputText(WebDriver driver) {
		element = driver.findElement(By.xpath("//textarea[@id='eduDetails']"));
		return element;

	}			
		
		
		
		
		
}