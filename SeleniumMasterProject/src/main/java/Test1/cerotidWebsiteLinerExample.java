
package Test1;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class cerotidWebsiteLinerExample {
	static WebDriver driver;

	public static void main(String[] args) {
		// Step 1: Invoke Browser go to cerotid page
		invokeBrowser();

		// Step 2: fill Form
		fillForm();

		// Step 3: Validate Success message
	}

	private static void fillForm() {
		// Select Course
		// Creating Webelement Object and finding the location of the element
		WebElement course = driver.findElement(By.xpath("//select[@id='classType']"));
		// Creating select object and passing the element
		Select chooseCourse = new Select(course);
		// creating a string varibale with course name
		String courseQaAutomation = "QA Automation";
		// selecting the course by visible text
		chooseCourse.selectByVisibleText(courseQaAutomation);

		// Selecting session
		WebElement session = driver.findElement(By.xpath("//select[@id='sessionType']"));
		Select chooseSession = new Select(session);
		String sessionType = "Upcoming Session";
		chooseSession.selectByVisibleText(sessionType);

		WebElement fullName = driver.findElement(By.xpath("//input[@id='name']"));
		fullName.sendKeys("Sanam Maharjan");
		
		WebElement address = driver.findElement(By.xpath("//input[@id='address']"));
		address.sendKeys("6001 Arlington Blvd");
		
		WebElement city = driver.findElement(By.xpath("//input[@id='city']"));
		city.sendKeys("Falls Church");
		
		WebElement state = driver.findElement(By.xpath("//select[@id='state']"));
		state.sendKeys("VA");
		
		WebElement zipcode = driver.findElement(By.xpath("//input[@id='zip']"));
		zipcode.sendKeys("22044");
		
		WebElement email = driver.findElement(By.xpath("//input[@id='email']"));
		email.sendKeys("snmpanga2014@gmail.com");
		
		WebElement phone = driver.findElement(By.xpath("//input[@id='phone']"));
		phone.sendKeys("2406458309");
		
		WebElement visaStatus = driver.findElement(By.xpath("//select[@id='visaStatus']"));
		visaStatus.sendKeys("GreenCard");
		
		WebElement mediaSource = driver.findElement(By.xpath("//select[@id='mediaSource']"));
		mediaSource.sendKeys("Friends/Family");
		
		WebElement relocate = driver.findElement(By.xpath("(//input[@id='relocate'])[2]"));
 		relocate.click();
 		
 		WebElement eduDetails = driver.findElement(By.xpath("//textarea[@id='eduDetails']"));
 		eduDetails.sendKeys("Master in Business Management");
 		
		
	}

	private static void invokeBrowser() {
		// Set the system path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// Chromedriver obj
		driver = new ChromeDriver();

		// Navigate to Cerotid Website
		driver.navigate().to("http://www.cerotid.com");

		System.out.println(driver.getTitle() + " -------------------- was launched");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}
}
