package Test1;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoolgeTest {

	public static void main(String[] args) throws InterruptedException {

		// Creating webdriver object
		// Setting the system path to utilize the chrome driver
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		

		// creating Webdriver object to drive my browser
		WebDriver driver = new ChromeDriver();
		// invoking the web browser
		driver.get("http://www.google.com");

		// maximize the browser
		driver.manage().window().maximize();

		WebElement txtBoxSearch = driver.findElement(By.xpath("//input[@name='q']"));
		txtBoxSearch.sendKeys("what is selenium in Java");

		WebElement searchBtn = driver.findElement(By.xpath("//input[@name='btnK']"));
		searchBtn.sendKeys(Keys.RETURN);
		
		driver.close();

	}
}
