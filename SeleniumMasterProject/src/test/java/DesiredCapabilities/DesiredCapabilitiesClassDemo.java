package DesiredCapabilities;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class DesiredCapabilitiesClassDemo {
	
	//1: How to Invoke Browser in Incognito mode(third party involvment)
	//2: Headless Mode(Hidden Broswer)
	//3: Add removal 
	
public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
	
	//Incognito
		/*
		 * ChromeOptions options = new ChromeOptions();
		 * options.addArguments("--incognito");
		 *  DesiredCapabilities capabilities = new DesiredCapabilities();
		 *  capabilities.setCapability(ChromeOptions.CAPABILITY,options); 
		 *  WebDriver driver = new ChromeDriver(options);
		 * driver.get("http://www.guru99.com/smoke-testing.html#7");
		 */
	
		/*
		 * //Head less Demo (Browser is invoked but in the background)
		 *  ChromeOptions options = new ChromeOptions(); 
		 * 
		 * options.addArguments("--headless");
		 * DesiredCapabilities capabilities = new DesiredCapabilities();
		 * capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		 * options.merge(capabilities);
		 * 
		 * WebDriver driver = new ChromeDriver(options);
		 * driver.get("http://www.guru99.com/smoke-testing.html#7");
		 * System.out.println("Navigated to------------" +driver.getTitle());
		 */
	//Ad blocker
	ChromeOptions options = new ChromeOptions();
	options.addExtensions(new File(".\\libs\\extension_4_10_0_0.crx"));
	DesiredCapabilities capabilities = new DesiredCapabilities();
	capabilities.setCapability(ChromeOptions.CAPABILITY, options);
	options.merge(capabilities);
	
	WebDriver driver = new ChromeDriver(options);
	driver.get("http://www.guru99.com/smoke-testing.html#7");
}
}
